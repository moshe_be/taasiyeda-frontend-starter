import { Component, OnInit } from '@angular/core';
import { ITodoItem } from '../interfaces/todo-item.interface';
import { TouchSequence } from 'selenium-webdriver';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-add-todo-item',
  templateUrl: './add-todo-item.component.html',
  styleUrls: ['./add-todo-item.component.css']
})
export class AddTodoItemComponent implements OnInit {

  todoList : ITodoItem[] = [];
  name : string = "moshe";
  todoItemName : string = '';
  constructor() { }

  ngOnInit() {
    this.changeName("bar");
  }

  changeName(newName : string) {
    console.log('change name called');
    this.name = newName;
  }

  setTodoItemName(newName : string) {
    this.todoItemName = newName;
  }

  addTodoItem() {
    this.todoList.push({
      message : this.todoItemName,
      isDone : false
    })
  }

}
