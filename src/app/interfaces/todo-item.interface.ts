export interface ITodoItem {
  message : string;
  isDone : boolean;
}
